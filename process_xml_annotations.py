import numpy as np

class RectLabelDataset(utils.Dataset):
    def load_training_images(self, dataset_dir, subset):
        dataset_dir = dataset_dir / subset
        annotation_dir = dataset_dir / "annotations"
        # Add classes. We have only one class to add
        # since this model only detects one kind of object.
        self.add_class(
            "custom_object",
            1,
            "custom_object"
            )
        # Load each image by finding all the
        # RectLabel annotation files and working
        # backward to the image. This is a lot
        # faster than having to load each image
        # into memory.
        count = 0
        for annotation_file in annotation_dir.glob("*.xml"):
            print(f"Parsing annotation: {annotation_file}")
            xml_text = annotation_file.read_text()
            annotation = xmltodict.parse(xml_text)['annotation']
            objects = annotation['object']
            image_filename = annotation['filename']
            if not isinstance(objects, list):
                objects = [objects]
            # Add the image to the data set
            self.add_image(
                source="custom_object",
                image_id=count,
                path=dataset_dir / image_filename,
                objects=objects,
                width=int(annotation["size"]['width']),
                height=int(annotation["size"]['height']),
            )
            count += 1
    
    def load_mask(self, image_id):
        # We have to generate our own bitmap
        # masks from the RectLabel polygons.
        # Look up the current image id
        info = self.image_info[image_id]
        # Create a blank mask the same size as
        # the image with as many depth channels
        # as there are annotations for this image.
        mask = np.zeros(
        [
        info["height"],
        info["width"],
        len(info["objects"])
        ],
        dtype=np.uint8
        )
        # Loop over each annotation for this image.
        # Each annotation will get its own channel
        # in the mask image.
        for i, o in enumerate(info["objects"]):
        # RectLabel uses Pascal VOC format which
        # is kind of wacky. We need to parse out
        # the x/y coordinates of each point that
        # make up the current polygon
        ys = []
        xs = []
        for label, number in o["polygon"].items():
        number = int(number)
        if label.startswith("x"):
        xs.append(number)
        else:
        ys.append(number)
        # Draw the filled polygon on top of the
        # mask image in the correct channel
        rr, cc = skimage.draw.polygon(ys, xs)
        mask[rr, cc, i] = 1
        # Return mask and array of class IDs of
        # each instance. Since we have one class
        # ID only, we return an array of 1s.
        return mask.astype(np.bool), np.ones(
        [mask.shape[-1]],
        dtype=np.int32
        )
    
    def image_reference(self, image_id):
        # Get the path for the image
        info = self.image_info[image_id]
        return info["path"]