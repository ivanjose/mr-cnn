(.env) bjalufka@Debian-CV:~/Python_Projects/r-cnn$ maskrcnn/bin/python train_model_1.py 
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:516: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint8 = np.dtype([("qint8", np.int8, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:517: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint8 = np.dtype([("quint8", np.uint8, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:518: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint16 = np.dtype([("qint16", np.int16, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:519: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint16 = np.dtype([("quint16", np.uint16, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:520: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint32 = np.dtype([("qint32", np.int32, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:525: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  np_resource = np.dtype([("resource", np.ubyte, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:541: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint8 = np.dtype([("qint8", np.int8, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:542: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint8 = np.dtype([("quint8", np.uint8, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:543: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint16 = np.dtype([("qint16", np.int16, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:544: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint16 = np.dtype([("quint16", np.uint16, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:545: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint32 = np.dtype([("qint32", np.int32, 1)])
/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:550: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  np_resource = np.dtype([("resource", np.ubyte, 1)])
Using TensorFlow backend.

Configurations:
BACKBONE                       resnet101
BACKBONE_STRIDES               [4, 8, 16, 32, 64]
BATCH_SIZE                     1
BBOX_STD_DEV                   [0.1 0.1 0.2 0.2]
COMPUTE_BACKBONE_SHAPE         None
DETECTION_MAX_INSTANCES        100
DETECTION_MIN_CONFIDENCE       0.9
DETECTION_NMS_THRESHOLD        0.3
FPN_CLASSIF_FC_LAYERS_SIZE     1024
GPU_COUNT                      1
GRADIENT_CLIP_NORM             5.0
IMAGES_PER_GPU                 1
IMAGE_MAX_DIM                  1024
IMAGE_META_SIZE                14
IMAGE_MIN_DIM                  800
IMAGE_MIN_SCALE                0
IMAGE_RESIZE_MODE              square
IMAGE_SHAPE                    [1024 1024    3]
LEARNING_MOMENTUM              0.9
LEARNING_RATE                  0.001
LOSS_WEIGHTS                   {'rpn_class_loss': 1.0, 'rpn_bbox_loss': 1.0, 'mrcnn_class_loss': 1.0, 'mrcnn_bbox_loss': 1.0, 'mrcnn_mask_loss': 1.0}
MASK_POOL_SIZE                 14
MASK_SHAPE                     [28, 28]
MAX_GT_INSTANCES               100
MEAN_PIXEL                     [123.7 116.8 103.9]
MINI_MASK_SHAPE                (56, 56)
NAME                           custom_object
NUM_CLASSES                    2
POOL_SIZE                      7
POST_NMS_ROIS_INFERENCE        1000
POST_NMS_ROIS_TRAINING         2000
ROI_POSITIVE_RATIO             0.33
RPN_ANCHOR_RATIOS              [0.5, 1, 2]
RPN_ANCHOR_SCALES              (32, 64, 128, 256, 512)
RPN_ANCHOR_STRIDE              1
RPN_BBOX_STD_DEV               [0.1 0.1 0.2 0.2]
RPN_NMS_THRESHOLD              0.7
RPN_TRAIN_ANCHORS_PER_IMAGE    256
STEPS_PER_EPOCH                100
TOP_DOWN_PYRAMID_SIZE          256
TRAIN_BN                       False
TRAIN_ROIS_PER_IMAGE           200
USE_MINI_MASK                  True
USE_RPN_ROIS                   True
VALIDATION_STEPS               50
WEIGHT_DECAY                   0.0001


WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/backend/tensorflow_backend.py:4070: The name tf.nn.max_pool is deprecated. Please use tf.nn.max_pool2d instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/tensorflow/python/ops/array_ops.py:1354: add_dispatch_support.<locals>.wrapper (from tensorflow.python.ops.array_ops) is deprecated and will be removed in a future version.
Instructions for updating:
Use tf.where in 2.0, which has the same broadcast rule as np.where
WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py:554: The name tf.random_shuffle is deprecated. Please use tf.random.shuffle instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/utils.py:200: The name tf.log is deprecated. Please use tf.math.log instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py:601: calling crop_and_resize_v1 (from tensorflow.python.ops.image_ops_impl) with box_ind is deprecated and will be removed in a future version.
Instructions for updating:
box_ind is deprecated, use box_indices instead
2020-03-17 07:59:44.798001: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2
2020-03-17 07:59:44.800463: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2111995000 Hz
2020-03-17 07:59:44.800553: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x10b42210 executing computations on platform Host. Devices:
2020-03-17 07:59:44.800576: I tensorflow/compiler/xla/service/service.cc:175]   StreamExecutor device (0): <undefined>, <undefined>
2020-03-17 07:59:45.463209: W tensorflow/compiler/jit/mark_for_compilation_pass.cc:1412] (One-time warning): Not using XLA:CPU for cluster because envvar TF_XLA_FLAGS=--tf_xla_cpu_global_jit was not set.  If you want XLA:CPU, either set that envvar, or use experimental_jit_scope to enable XLA:CPU.  To confirm that XLA is active, pass --vmodule=xla_compilation_cache=1 (as a proper command-line flag, not via TF_XLA_FLAGS) or set the envvar XLA_FLAGS=--xla_hlo_profile.

Starting at epoch 0. LR=0.001

Checkpoint Path: training_logs/custom_object20200317T0759/mask_rcnn_custom_object_{epoch:04d}.h5
Selecting layers to train
fpn_c5p5               (Conv2D)
fpn_c4p4               (Conv2D)
fpn_c3p3               (Conv2D)
fpn_c2p2               (Conv2D)
fpn_p5                 (Conv2D)
fpn_p2                 (Conv2D)
fpn_p3                 (Conv2D)
fpn_p4                 (Conv2D)
In model:  rpn_model
    rpn_conv_shared        (Conv2D)
    rpn_class_raw          (Conv2D)
    rpn_bbox_pred          (Conv2D)
mrcnn_mask_conv1       (TimeDistributed)
mrcnn_mask_bn1         (TimeDistributed)
mrcnn_mask_conv2       (TimeDistributed)
mrcnn_mask_bn2         (TimeDistributed)
mrcnn_class_conv1      (TimeDistributed)
mrcnn_class_bn1        (TimeDistributed)
mrcnn_mask_conv3       (TimeDistributed)
mrcnn_mask_bn3         (TimeDistributed)
mrcnn_class_conv2      (TimeDistributed)
mrcnn_class_bn2        (TimeDistributed)
mrcnn_mask_conv4       (TimeDistributed)
mrcnn_mask_bn4         (TimeDistributed)
mrcnn_bbox_fc          (TimeDistributed)
mrcnn_mask_deconv      (TimeDistributed)
mrcnn_class_logits     (TimeDistributed)
mrcnn_mask             (TimeDistributed)
WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/backend/tensorflow_backend.py:422: The name tf.global_variables is deprecated. Please use tf.compat.v1.global_variables instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/backend/tensorflow_backend.py:431: The name tf.is_variable_initialized is deprecated. Please use tf.compat.v1.is_variable_initialized instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/backend/tensorflow_backend.py:438: The name tf.variables_initializer is deprecated. Please use tf.compat.v1.variables_initializer instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/callbacks/tensorboard_v1.py:200: The name tf.summary.merge_all is deprecated. Please use tf.compat.v1.summary.merge_all instead.

WARNING:tensorflow:From /home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/callbacks/tensorboard_v1.py:203: The name tf.summary.FileWriter is deprecated. Please use tf.compat.v1.summary.FileWriter instead.

ERROR:root:Error processing image {'id': 'IMG_20200304_161042.jpg', 'source': 'handle', 'path': 'training_images/training_set_1/IMG_20200304_161042.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [1064, 2515, 2535, 2637, 2663, 3223, 3253, 3335, 3452, 3543, 3609, 3594, 3553, 3457, 3355, 3228, 3187, 2642, 2607, 2535, 2510, 1054, 972, 937, 901, 860, 860, 876, 896, 927, 947, 988, 1049], 'all_points_y': [1115, 1115, 1135, 1176, 1227, 1268, 1232, 1217, 1212, 1268, 1395, 1543, 1634, 1700, 1705, 1675, 1655, 1660, 1731, 1726, 1777, 1777, 1726, 1655, 1624, 1517, 1369, 1288, 1247, 1247, 1176, 1140, 1135]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3429 is out of bounds for axis 1 with size 3024
Epoch 1/8
ERROR:root:Error processing image {'id': 'IMG_20200304_160933.jpg', 'source': 'handle', 'path': 'training_images/training_set_1/IMG_20200304_160933.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [1349, 2556, 2642, 2668, 3024, 3044, 3141, 3141, 3228, 3319, 3396, 3462, 3508, 3497, 3436, 3401, 3279, 3192, 3156, 3136, 3039, 3009, 2627, 2607, 2515, 1344, 1298, 1252, 1222, 1212, 1247, 1319], 'all_points_y': [1084, 1044, 1084, 1130, 1166, 1151, 1151, 1181, 1145, 1135, 1140, 1156, 1181, 1588, 1624, 1639, 1644, 1639, 1614, 1629, 1609, 1609, 1619, 1639, 1670, 1609, 1583, 1502, 1400, 1268, 1181, 1115]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3310 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161029.jpg', 'source': 'handle', 'path': 'training_images/training_set_1/IMG_20200304_161029.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [1329, 2820, 2948, 3049, 3146, 3223, 3284, 3294, 3253, 3167, 3029, 2856, 2617, 2479, 2367, 1125, 1100, 1028, 998, 957, 911, 911, 972, 1069, 1115, 1145], 'all_points_y': [835, 748, 815, 881, 967, 1084, 1257, 1481, 1731, 1940, 2077, 2194, 2245, 2245, 2209, 1527, 1456, 1405, 1380, 1308, 1227, 1079, 993, 947, 927, 962]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3024 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161100.jpg', 'source': 'handle', 'path': 'training_images/validation_set_1/IMG_20200304_161100.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [960, 1000, 1156, 1254, 1729, 1725, 1796, 1796, 3005, 3081, 3156, 3232, 3307, 3379, 3401, 3392, 3379, 3343, 2143, 2067, 2000, 1965, 1445, 1414, 1347, 1231, 1138, 1040], 'all_points_y': [1867, 1783, 1680, 1671, 1338, 1294, 1231, 1191, 467, 440, 462, 498, 578, 689, 800, 902, 960, 1027, 1765, 1729, 1774, 1725, 2009, 2054, 2129, 2165, 2187, 2054]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3079 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161625.jpg', 'source': 'handle', 'path': 'training_images/training_set_1/IMG_20200304_161625.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [1639, 2449, 2469, 2505, 2810, 2871, 2922, 2993, 3055, 3080, 3049, 2958, 2902, 2846, 2637, 2632, 2591, 1761, 1700, 1665, 1644, 1614, 1609, 1634], 'all_points_y': [1634, 1303, 1313, 1313, 1232, 1201, 1171, 1181, 1273, 1339, 1400, 1456, 1466, 1487, 1578, 1599, 1634, 1945, 1929, 1899, 1909, 1828, 1705, 1690]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3024 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161100.jpg', 'source': 'handle', 'path': 'training_images/validation_set_1/IMG_20200304_161100.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [960, 1000, 1156, 1254, 1729, 1725, 1796, 1796, 3005, 3081, 3156, 3232, 3307, 3379, 3401, 3392, 3379, 3343, 2143, 2067, 2000, 1965, 1445, 1414, 1347, 1231, 1138, 1040], 'all_points_y': [1867, 1783, 1680, 1671, 1338, 1294, 1231, 1191, 467, 440, 462, 498, 578, 689, 800, 902, 960, 1027, 1765, 1729, 1774, 1725, 2009, 2054, 2129, 2165, 2187, 2054]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3079 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161100.jpg', 'source': 'handle', 'path': 'training_images/validation_set_1/IMG_20200304_161100.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [960, 1000, 1156, 1254, 1729, 1725, 1796, 1796, 3005, 3081, 3156, 3232, 3307, 3379, 3401, 3392, 3379, 3343, 2143, 2067, 2000, 1965, 1445, 1414, 1347, 1231, 1138, 1040], 'all_points_y': [1867, 1783, 1680, 1671, 1338, 1294, 1231, 1191, 467, 440, 462, 498, 578, 689, 800, 902, 960, 1027, 1765, 1729, 1774, 1725, 2009, 2054, 2129, 2165, 2187, 2054]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3079 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161100.jpg', 'source': 'handle', 'path': 'training_images/validation_set_1/IMG_20200304_161100.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [960, 1000, 1156, 1254, 1729, 1725, 1796, 1796, 3005, 3081, 3156, 3232, 3307, 3379, 3401, 3392, 3379, 3343, 2143, 2067, 2000, 1965, 1445, 1414, 1347, 1231, 1138, 1040], 'all_points_y': [1867, 1783, 1680, 1671, 1338, 1294, 1231, 1191, 467, 440, 462, 498, 578, 689, 800, 902, 960, 1027, 1765, 1729, 1774, 1725, 2009, 2054, 2129, 2165, 2187, 2054]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3079 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161631.jpg', 'source': 'handle', 'path': 'training_images/training_set_1/IMG_20200304_161631.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [1583, 2591, 2668, 2678, 2948, 2999, 3070, 3111, 3151, 3151, 3095, 3014, 2922, 2820, 2790, 2464, 2423, 2388, 2311, 1222, 1217, 1232, 1207, 1222, 1283, 1339, 1410, 1436, 1466, 1517], 'all_points_y': [1176, 1858, 1945, 2001, 2204, 2204, 2240, 2286, 2377, 2464, 2551, 2612, 2612, 2581, 2510, 2321, 2337, 2316, 2316, 1599, 1558, 1487, 1461, 1385, 1293, 1232, 1181, 1207, 1171, 1171]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3024 is out of bounds for axis 1 with size 3024
2020-03-17 08:02:21.689667: W tensorflow/core/framework/allocator.cc:107] Allocation of 603979776 exceeds 10% of system memory.
2020-03-17 08:02:29.584766: W tensorflow/core/framework/allocator.cc:107] Allocation of 603979776 exceeds 10% of system memory.
2020-03-17 08:02:36.007401: W tensorflow/core/framework/allocator.cc:107] Allocation of 603979776 exceeds 10% of system memory.
2020-03-17 08:02:40.036802: W tensorflow/core/framework/allocator.cc:107] Allocation of 603979776 exceeds 10% of system memory.
  1/100 [..............................] - ETA: 4:04:13 - loss: 4.6659ERROR:root:Error processing image {'id': 'IMG_20200304_161100.jpg', 'source': 'handle', 'path': 'training_images/validation_set_1/IMG_20200304_161100.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [960, 1000, 1156, 1254, 1729, 1725, 1796, 1796, 3005, 3081, 3156, 3232, 3307, 3379, 3401, 3392, 3379, 3343, 2143, 2067, 2000, 1965, 1445, 1414, 1347, 1231, 1138, 1040], 'all_points_y': [1867, 1783, 1680, 1671, 1338, 1294, 1231, 1191, 467, 440, 462, 498, 578, 689, 800, 902, 960, 1027, 1765, 1729, 1774, 1725, 2009, 2054, 2129, 2165, 2187, 2054]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3079 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161625.jpg', 'source': 'handle', 'path': 'training_images/training_set_1/IMG_20200304_161625.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [1639, 2449, 2469, 2505, 2810, 2871, 2922, 2993, 3055, 3080, 3049, 2958, 2902, 2846, 2637, 2632, 2591, 1761, 1700, 1665, 1644, 1614, 1609, 1634], 'all_points_y': [1634, 1303, 1313, 1313, 1232, 1201, 1171, 1181, 1273, 1339, 1400, 1456, 1466, 1487, 1578, 1599, 1634, 1945, 1929, 1899, 1909, 1828, 1705, 1690]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3024 is out of bounds for axis 1 with size 3024
ERROR:root:Error processing image {'id': 'IMG_20200304_161100.jpg', 'source': 'handle', 'path': 'training_images/validation_set_1/IMG_20200304_161100.jpg', 'width': 3024, 'height': 4032, 'polygons': [{'name': 'polygon', 'all_points_x': [960, 1000, 1156, 1254, 1729, 1725, 1796, 1796, 3005, 3081, 3156, 3232, 3307, 3379, 3401, 3392, 3379, 3343, 2143, 2067, 2000, 1965, 1445, 1414, 1347, 1231, 1138, 1040], 'all_points_y': [1867, 1783, 1680, 1671, 1338, 1294, 1231, 1191, 467, 440, 462, 498, 578, 689, 800, 902, 960, 1027, 1765, 1729, 1774, 1725, 2009, 2054, 2129, 2165, 2187, 2054]}]}
Traceback (most recent call last):
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3079 is out of bounds for axis 1 with size 3024
2020-03-17 08:03:41.092979: W tensorflow/core/framework/allocator.cc:107] Allocation of 603979776 exceeds 10% of system memory.
 47/100 [=============>................] - ETA: 41:54 - loss: 2.5721multiprocessing.pool.RemoteTraceback: 
"""
Traceback (most recent call last):
  File "/usr/lib/python3.7/multiprocessing/pool.py", line 121, in worker
    result = (True, func(*args, **kwds))
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/utils/data_utils.py", line 650, in next_sample
    return six.next(_SHARED_SEQUENCES[uid])
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1704, in data_generator
    use_mini_mask=config.USE_MINI_MASK)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 1219, in load_image_gt
    mask, class_ids = dataset.load_mask(image_id)
  File "train_model_1.py", line 126, in load_mask
    mask[rr, cc, i] = 1
IndexError: index 3024 is out of bounds for axis 1 with size 3024
"""

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "train_model_1.py", line 180, in <module>
    train(model)
  File "train_model_1.py", line 164, in train
    layers='heads'
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/mrcnn/model.py", line 2353, in train
    use_multiprocessing=True,
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/legacy/interfaces.py", line 91, in wrapper
    return func(*args, **kwargs)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/engine/training.py", line 1732, in fit_generator
    initial_epoch=initial_epoch)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/engine/training_generator.py", line 185, in fit_generator
    generator_output = next(output_generator)
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/utils/data_utils.py", line 742, in get
    six.reraise(*sys.exc_info())
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/six.py", line 703, in reraise
    raise value
  File "/home/bjalufka/Python_Projects/r-cnn/maskrcnn/lib/python3.7/site-packages/keras/utils/data_utils.py", line 711, in get
    inputs = future.get(timeout=30)
  File "/usr/lib/python3.7/multiprocessing/pool.py", line 657, in get
    raise self._value
IndexError: index 3024 is out of bounds for axis 1 with size 3024
