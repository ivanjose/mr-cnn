This project runs in a separate Python virtual environment than the other projects since it requires a different
version of TensorFlow to run the Mask-RCNN model.

If you are running this project from within PyCharm, make sure that the 'Run Configuration' for this script
uses the 'Python 3.6 (maskrcnn)' virtual environment instead of the 'Project Default (Python 3.6)' interpreter.

If you are running this project from the command line, you need to first activate the virtual environment:

source ~/maskrcnn/bin/activate

When you are done running this example, run the following command to deactivate the virtual environment:

deactivate

If you get a MemoryError or OOM (Out of Memory) error while running this project, you need to increase the amount of
available RAM for the virtual machine in VMWare. After increasing the amount of RAM available to the VM in VMWare
settings, restart the VM, log in and try running the project again.