import json

# Open Json file and extract image metadata annotations
with open("training_images/training_set_1/annotations/training_set_annotations.json", "r") as file:
    img_data = json.load(file)['_via_img_metadata']
    
img_file_data = []

img_filename = []
img_size = []
img_regions = []

xs = [[]]
ys = [[]]
corner = [[]]
widths = [[]]
heights = [[]]
i = 0
# Iterate over each file data and extract relevant data
for img_file in img_data:
    img_file_data.append(img_data[img_file])
    img_filename.append(img_data[img_file]['filename'])
    img_size.append(img_data[img_file]['size'])
    img_regions.append(img_data[img_file]['regions'])
    
    # img_file_data has 'filename', 'size', and 'regions' fields.
    for region in img_regions:
        if len(region['shape_attributes']) == 3: # Polygon Mask
            xs[i].append(region['shape_attributes']['all_points_x'])
            ys[i].append(region['shape_attributes']['all_points_y'])
        else: # Rectangle
            corner[i].append(region['shape_attributes']['x'], region['shape_attribures']['y'])
            widths[i].append(region['shape_attributes']['width'])
            heights[i].append(region['shape_attribures']['height'])
            

with open("/home/bjalufka/Python_Projects/r-cnn/training_images/training_set_1/annotations/via_export_coco.json", "r") as file:
    json_text = json.load(file)
    
images = json_text['images']
poligonx = json_text['annotations']