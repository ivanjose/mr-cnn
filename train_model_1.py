# This demo is roughly based on the balloon.py demo included with Matterport's Mask R-CNN implementation which
# is licensed under the MIT License (see LICENSE for details).
# This is based on code originally written by Waleed Abdulla but re-written to support data annotated by RectLabel.

import warnings
from pathlib import Path
import os
import json
import numpy as np
import skimage.draw
from mrcnn.config import Config
from mrcnn import model as modellib, utils
from PIL import Image

ROOT_DIR = Path(".")
COCO_WEIGHTS_PATH = ROOT_DIR / "mask_rcnn_coco.h5"

# Download COCO trained weights if you don't already have them.
if not COCO_WEIGHTS_PATH.exists():
    utils.download_trained_weights(str(COCO_WEIGHTS_PATH))

# Directory to save logs and model checkpoints
DEFAULT_LOGS_DIR = ROOT_DIR / "training_logs"

# Where the training images and annotation files live
DATASET_PATH = ROOT_DIR / "training_images"

# Start training from the pre-trained COCO model. You can change this path if you want to pick up training from a prior
# checkpoint file in your ./logs folder.
WEIGHTS_TO_START_FROM = COCO_WEIGHTS_PATH


class ObjectDetectorConfig(Config):
    NAME = "custom_object"
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 1  # Background + your custom object
    STEPS_PER_EPOCH = 100

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.9


class HandleDataset(utils.Dataset):

    def load_training_images(self, dataset_dir, subset):
        """Load a subset of the Handle dataset.
        dataset_dir: Root directory of the dataset.
        subset: Subset to load: train or val
        """
        
        dataset_dir = dataset_dir / subset
        annotation_dir = dataset_dir / "annotations"
        
        # Add classes. We have only one class to add.
        self.add_class("handle", 1, "handle")

        # Load annotations
        # VGG Image Annotator (up to version 1.6) saves each image in the form:
        # { 'filename': '28503151_5b5b7ec140_b.jpg',
        #   'regions': {
        #       '0': {
        #           'region_attributes': {},
        #           'shape_attributes': {
        #               'all_points_x': [...],
        #               'all_points_y': [...],
        #               'name': 'polygon'}},
        #       ... more regions ...
        #   },
        #   'size': 100202
        # }
        # We mostly care about the x and y coordinates of each region
        # Note: In VIA 2.0, regions was changed from a dict to a list.
        subset_str = subset[:len(subset)-1]
        annotations = json.load(open(f"{annotation_dir}/{subset_str}annotations.json"))['_via_img_metadata']
        annotations = list(annotations.values())  # don't need the dict keys

        # The VIA tool saves images in the JSON even if they don't have any
        # annotations. Skip unannotated images.
        annotations = [a for a in annotations if a['regions']]

        # Add images
        for a in annotations:
            # Get the x, y coordinaets of points of the polygons that make up
            # the outline of each object instance. These are stores in the
            # shape_attributes (see json format above)
            # The if condition is needed to support VIA versions 1.x and 2.x.
            if type(a['regions']) is dict:
                polygons = [r['shape_attributes'] for r in a['regions'].values()]
            else:
                polygons = [r['shape_attributes'] for r in a['regions']] 

            # load_mask() needs the image size to convert polygons to masks.
            # Unfortunately, VIA doesn't include it in JSON, so we must read
            # the image. This is only managable since the dataset is tiny.
            image_path = os.path.join(dataset_dir, a['filename'])
            image = skimage.io.imread(image_path)
            height, width = image.shape[:2]

            self.add_image(
                "handle",
                image_id=a['filename'],  # use file name as a unique image id
                path=image_path,
                width=width, height=height,
                polygons=polygons)

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        # If not a handle dataset image, delegate to parent class.
        image_info = self.image_info[image_id]
        if image_info["source"] != "handle":
            return super(self.__class__, self).load_mask(image_id)

        # Convert polygons to a bitmap mask of shape
        # [height, width, instance_count]
        info = self.image_info[image_id]
        mask = np.zeros([info["height"], info["width"], len(info["polygons"])],
                        dtype=np.uint8)
        for i, p in enumerate(info["polygons"]):
            # Get indexes of pixels inside the polygon and set them to 1
            rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            mask[rr, cc, i] = 1

        # Return mask, and array of class IDs of each instance. Since we have
        # one class ID only, we return an array of 1s
        return mask.astype(np.bool), np.ones([mask.shape[-1]], dtype=np.int32)

    def image_reference(self, image_id):
        """Return the path of the image."""
        info = self.image_info[image_id]
        if info["source"] == "handle":
            return info["path"]
        else:
            super(self.__class__, self).image_reference(image_id)


def train(model):
    # Load the training data set
    dataset_train = HandleDataset()
    dataset_train.load_training_images(DATASET_PATH, "training_set_1")
    dataset_train.prepare()

    # Load the validation data set
    dataset_val = HandleDataset()
    dataset_val.load_training_images(DATASET_PATH, "validation_set_1")
    dataset_val.prepare()

    with warnings.catch_warnings():
        # Suppress annoying skimage warning due to code inside Mask R-CNN.
        # Not needed, but makes the output easier to read until Mask R-CNN is updated.
        warnings.simplefilter("ignore")

        # Re-train the model on a small data set. If you are training from scratch with a huge data set,
        # you'd want to train longer and customize these settings.
        model.train(
            dataset_train,
            dataset_val,
            learning_rate=config.LEARNING_RATE,
            epochs=8,
            layers='heads'
        )


# Load config
config = ObjectDetectorConfig()
config.display()

# Create the model
model = modellib.MaskRCNN(mode="training", config=config, model_dir=DEFAULT_LOGS_DIR)

# Load the weights we are going to start with
# Note: If you are picking up from a training checkpoint instead of the COCO weights, remove the excluded layers.
model.load_weights(str(WEIGHTS_TO_START_FROM), by_name=True, exclude=["mrcnn_class_logits", "mrcnn_bbox_fc", "mrcnn_bbox", "mrcnn_mask"])

# Run the training process
train(model)
